#include <stdio.h>

/* print Fahrenhei-Celsius table for fahr =0, 20, ..., 300 */
#define LOWER   0     /* lower limit of table */
#define UPPER   300
#define STEP    20

void whileC_F(void);
int main()
{
    int fahr;
    for (fahr = UPPER; fahr>=LOWER; fahr-=STEP)
        printf("%3d\t%6.2f\n", fahr, (5/9.0)*(fahr-32));
    
    
    /* whileC_F(); */
}


void whileC_F(void)
{
    float fahr, celsius;
    fahr = LOWER;

    printf("Fahrenheit\tCelsius\n");

    while (fahr <= UPPER) {
        /* celsius = 5 * (fahr-32) / 9; */
        celsius = (5.0/9.0) * (fahr-32.0);
        printf("%3.f\t\t%6.2f\n", fahr, celsius);
        fahr += STEP;
    }
}
