#include <stdio.h>

/* count characters in input; 1st version */

void CountChar0(void);
void CountChar1(void);
void CountLines0(void);
void CountBTN(void);
void CopyReplace0(void);
void CopyReplace1(void);
void wordsCount0(void);
int main(void)
{
    /*
    CountChar0(); 
    CountChar1();
    CountLines0();
    CountBTN();
    CopyReplace0();
    CopyReplace1();
    */
    wordsCount0();
}

void CountChar0(void)
{
    long nc;
    
    nc = 0;
    while(getchar() != EOF) ++nc; printf("%ld\n", nc);
}

void CountChar1(void)
{
    double nc;

    for (nc = 0; getchar() != EOF; ++nc); printf("%.0f\n", nc);
}

void CountLines0(void)
{ 
    int c, nl;
    
    nl = 0;
    while ((c = getchar()) != EOF) if (c == '\n') ++nl;
    printf(">>%d\n", nl);
}

void CountBTN(void)
{
    int blank = 0, tab = 0, newl = 0, c;
    
    while ((c = getchar()) != EOF)
        if (c == '\t') ++tab;
        else if (c == '\n') ++newl;
        else if (c == ' ') ++blank;
    printf("\n>T = %d\n>NL = %d\nB = %d\n", tab, newl, blank);
}

void CopyReplace0(void)
{
    int c, blankChar = 0;
    while ((c = getchar()) != EOF) {
       /* if (c == ' '  && blankChar) continue;
        putchar(c);
        blankChar = c == ' ';*/
    }
}

void CopyReplace1(void)
{
    int c, replace = 0;
    while ((c = getchar()) != EOF){
        if (c == '\t') {printf("%s", "\\t");}
        else if (c == '\b') {printf("%s", "\\b");}
        else if (c == '\\') {printf("%s", "\\\\");}
        else {printf("%c", c);}
    }
}

#define IN 1    /* inside a word */
#define OUT 0   /* outside a word */

/* count lines, words. and characters in input */

void wordsCount0(void)
{
    int c,  nl, nw, nc, state;

    state = OUT;
    nl = nw = nc = 0;
    while ((c = getchar()) != EOF){
        ++nc;
        if (c == '\n') ++nl;
        if (c == ' ' || c == '\n' || c == '\t') state = OUT;
        else if (state == OUT){ state = IN; ++nw; }
    }
    printf("%d %d %d\n", nl, nw, nc);
}
